from cProfile import Profile
from pstats import SortKey, Stats
from nunchaku import Nunchaku
from tests.test_nunchaku import generate_samples, add_noise

def run_nunchaku():
    X, Y, M, theta = generate_samples(N_min=10, N_max=10)
    Y_hat = add_noise(Y, scale=1, rep=20)
    nc = Nunchaku(X, Y_hat, estimate_err=True, prior=[-25, 25])
    M_hat, _ = nc.get_number(15)
    bds, _ = nc.get_iboundaries(M_hat)
    info_df = nc.get_info(bds)
    return M_hat


with Profile() as profile:
    print(f"estimated M = {run_nunchaku()}.")
    (
        Stats(profile)
        .strip_dirs()
        .sort_stats(SortKey.CUMULATIVE)
        .print_stats()
    )
