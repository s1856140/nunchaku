import unittest
import numpy as np
from nunchaku import Nunchaku
from nunchaku import GibbsSampler
from tests.test_nunchaku import generate_samples, add_noise


class TestGibbsSampler(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        X, Y, M, theta = generate_samples(N_max=5)
        Y_hat = add_noise(Y, scale=0.2)
        # data
        cls.X = X
        cls.Y = Y_hat
        cls.M = M
        cls.theta = theta

    @classmethod
    def tearDownClass(cls):
        cls.X = None
        cls.Y = None
        cls.M = None
        cls.theta = None

    def test_gibbs_sampler(self):
        nc = Nunchaku(self.X, self.Y)
        gs = GibbsSampler(nc)
        bd_max, samples = gs.sample(
            num_steps=5000,
            random_steps=500,
            random_attempts=5,
            return_samples=True,
            plot_traces=True,
            plot_hist=True,
        )
        gs.plot_gibbs_traces(samples)
