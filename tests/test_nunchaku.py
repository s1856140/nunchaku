import unittest
import numpy as np
from nunchaku import Nunchaku
from scipy.special import loggamma


def generate_samples(
    N_min=1, N_max=10, L_min=10, L_max=50, min_degree=20, slope_range=20
):
    N = np.random.choice(list(range(N_min, N_max + 1)))
    L = np.random.choice(list(range(L_min, L_max + 1)), N)
    # generate random angle between slope_range
    deg_range = np.arctan(slope_range)
    theta = [2 * deg_range * np.random.random() - deg_range]
    point = np.array([[0, 0]])
    dx = 1
    x, y = list(point[0])
    # generate ground truth
    for j in range(N):
        while True:
            new_theta = 2 * deg_range * np.random.random() - deg_range
            if abs((new_theta - theta[-1])) < min_degree / 180 * np.pi:
                continue
            else:
                theta.append(new_theta)
                break
        slope = np.tan(new_theta)
        for k in range(L[j]):
            dy = slope * dx
            x += dx
            y += dy
            point = np.append(point, [[x, y]], axis=0)
    x = point[:, 0]
    y = point[:, 1]
    # normalise such that min is zero
    y = y - np.min(y)
    return x, y, N, theta[1:]


def add_noise(y, scale=1, rep=10):
    noise = np.random.normal(scale=scale, size=(rep, len(y)))
    y_hat = y + noise
    return y_hat


def norm_matrix(matrix):
    mat = matrix.copy().astype(np.longdouble)
    norm = np.nanmax(mat)
    mat = np.exp(mat - norm)
    return mat


def sum_for_four_regions(mat, N, minlen):
    evi = 0
    for N1 in range(minlen - 1, N - 3 * minlen):
        for N2 in range(N1 + minlen, N - 2 * minlen):
            for N3 in range(N2 + minlen, N - minlen):
                evi += (
                    mat[0, N1] * mat[N1 + 1, N2] * mat[N2 + 1, N3] * mat[N3 + 1, N - 1]
                )
    # returns the evidence at linear scale, normalised by the matrix's max element
    return evi


def second_moment_N2_for_four_regions(mat, N, minlen):
    evi = 0
    for N1 in range(minlen - 1, N - 3 * minlen):
        for N2 in range(N1 + minlen, N - 2 * minlen):
            for N3 in range(N2 + minlen, N - minlen):
                evi += (
                    N2**2
                    * mat[0, N1]
                    * mat[N1 + 1, N2]
                    * mat[N2 + 1, N3]
                    * mat[N3 + 1, N - 1]
                )
    # returns the evidence at linear scale, normalised by the matrix's max element
    return evi


class TestNunchaku(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        X, Y, M, theta = generate_samples(N_min=4, N_max=4)
        cls.scale = 0.2
        Y_hat = add_noise(Y, scale=cls.scale)
        # data
        cls.X = X
        cls.Y = Y_hat
        cls.M = M
        cls.theta = theta

    @classmethod
    def tearDownClass(cls):
        cls.X = None
        cls.Y = None
        cls.M = None
        cls.theta = None

    def test_pipeline(self):
        # give yrange
        ymax = 5 * 50 * 20
        nc = Nunchaku(self.X, self.Y, yrange=[0, ymax])
        # give prior
        nc = Nunchaku(self.X, self.Y, prior=[-25, 25])
        # ability to find correct answer
        M_hat, _ = nc.get_number(15)
        assert M_hat == self.M, f"wrong answer: {M_hat} != ground truth {self.M}"
        bds, _ = nc.get_iboundaries(M_hat)
        slopes_hat = nc.get_info(bds)["gradient"].to_numpy()
        slopes = np.tan(np.array(self.theta))
        score = np.abs((slopes_hat - slopes) / slopes).mean() * 100
        assert score < 10
        # plots
        info_df = nc.get_info(bds)
        nc.plot(info_df)
        # maths of findZ
        mat = norm_matrix(nc.evidence)
        Z2 = sum_for_four_regions(mat, len(nc.x), nc.minlen)
        E2 = second_moment_N2_for_four_regions(mat, len(nc.x), nc.minlen)
        mat[np.isnan(mat)] = 0  # set nan to zero for _findZ to work
        Z1 = nc._findZ(mat, 4)
        E1 = nc._find_moment(mat, 4, k=2, moment=2)
        # error must be smaller than 0.0001%
        assert np.abs((Z2 - Z1) / Z2) * 100 < 0.0001
        assert np.abs((E2 - E1) / E2) * 100 < 0.0001

    def test_pipeline_sigma(self):
        nc = Nunchaku(self.X, self.Y, prior=[-25, 25], estimate_err=False)
        # ability to find correct answer
        M_hat, _ = nc.get_number(8)
        assert M_hat == self.M
        bds, _ = nc.get_iboundaries(M_hat)
        slopes_hat = nc.get_info(bds)["gradient"].to_numpy()
        slopes = np.tan(np.array(self.theta))
        score = np.abs((slopes_hat - slopes) / slopes).mean() * 100
        print(
            f"data error MLE: {nc.get_MLE_of_error(M_hat)}, ground truth: {self.scale}"
        )
        assert score < 10
        # plots
        info_df = nc.get_info(bds)
        nc.plot(info_df)

    def test_maths(self):
        # sigma is known, test recursive sum
        nc = Nunchaku(self.X, self.Y, prior=[-25, 25])
        mat = norm_matrix(nc.evidence)
        Z2 = sum_for_four_regions(mat, len(nc.x), nc.minlen)
        E2 = second_moment_N2_for_four_regions(mat, len(nc.x), nc.minlen)
        mat[np.isnan(mat)] = 0  # set nan to zero for _findZ to work
        Z1 = nc._findZ(mat, 4)
        E1 = nc._find_moment(mat, 4, k=2, moment=2)
        # error must be smaller than 1%
        assert np.abs((Z2 - Z1) / Z2) * 100 < 0.01
        assert np.abs((E2 - E1) / E2) * 100 < 0.01

        # sigma is unknown, test accuracy of quad
        nc = Nunchaku(self.X, self.Y.mean(axis=0), prior=[-25, 25], estimate_err=False)
        for M in [2, 4]:
            Z1 = nc._findZ_unknown_err_numerical(M)
            Z2 = nc._findZ_unknown_err_analytical(M) + loggamma(
                (len(nc.x) * nc.nreplicates - 1) / 2 - M
            )
            E1 = nc._find_moment_unknown_err_numerical(number=M, k=M - 1, moment=2)
            E2 = nc._find_moment_unknown_err_analytical(
                number=M, k=M - 1, moment=2
            ) + loggamma((len(nc.x) * nc.nreplicates - 1) / 2 - M)
            assert np.abs((Z2 - Z1) / Z2) * 100 < 0.01
            assert np.abs((E2 - E1) / E2) * 100 < 0.01

    def test_poly(self):
        # generate a 3rd-order polynomial
        x = np.arange(30)
        y1 = x[:10] ** 3 + 1
        y2 = y1[-1] - (-x[:10] ** 2 + 2 * x[:10])
        y3 = y2[-1] + x[:10] * 30
        y = np.concatenate([y1, y2, y3], axis=None)

        # three replicates of noise
        y = y + np.random.normal(0, 2, (3, 30))

        # inference
        bases = [np.ones_like, lambda x: x, lambda x: x**2, lambda x: x**3]
        prior = [[-100, 100], [-10, 10], [-5, 5], [-1, 1]]
        nc = Nunchaku(x, y, prior=prior, bases=bases)
        N_stm, _ = nc.get_number(5)
        assert N_stm == 3
        bds, _ = nc.get_iboundaries(3)
        print(f"boundaries for polyonmial: {bds}")
        nc.plot(nc.get_info(bds), show=False)

    def test_sine(self):
        # generate a piece-wise sine function
        x = np.linspace(0, 5*np.pi, 240)
        y1 = 2 * np.sin(2 * x[:80]) + 3
        y2 = 3 * np.sin(3 * x[80:120]) + 3
        y3 = 3 * np.sin(2.5 * x[120:]) + 3
        y = np.concatenate([y1, y2, y3], axis=None)

        # three replicates of noise
        y_hat = y + np.random.normal(0, 0.25, (3, 240))

        # inference
        bases = [np.ones_like, lambda x: np.sin(2 * x), lambda x: np.sin(2.5 * x), lambda x: np.sin(3 * x)]
        prior = [[-10, 10], [-10, 10], [-10, 10], [-10, 10]]
        nc = Nunchaku(x, y_hat, prior=prior, bases=bases)
        N, _ = nc.get_number(5)
        bds, _ = nc.get_iboundaries(N)
        print(f"boundaries for piecewise-sine: {bds}")
        nc.plot(nc.get_info(bds), show=False)
